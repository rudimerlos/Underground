# ---------------------------------------------------------------------------
#   Copyright (C) 2021  Rudi Merlos Carrión
#
#   This file is part of Underground.
#
#   Underground is free software: you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   Underground is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with Underground. If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------------

# ===========================================================================
#   TILEMAP AND CAMERA CLASSES FOR UNDERGROUND GAME
# ===========================================================================

import pygame
import pytmx
from settings import *


class TileMap:
    ''' Tilemap manager. '''
    def __init__(self, game, filename):
        ''' Load tilemap and set his size. '''
        tm = pytmx.load_pygame(filename, pixelalpha=True)
        self.width = tm.width * tm.tilewidth * game.scale
        self.height = tm.height * tm.tileheight * game.scale
        self.tmxdata = tm
        self.game = game
        # Calculate and fill levels coords
        self.levels_row = self.height // game.height
        self.levels_col = self.width // game.width
        self.levels = []
        for i in range(self.levels_row):
            self.levels.append([])
            for j in range(self.levels_col):
                self.levels[i].append(vec(j * game.width, i * game.height))

    def render(self, surface):
        ''' Render tiles in surface. '''
        ti = self.tmxdata.get_tile_image_by_gid
        for layer in self.tmxdata.visible_layers:
            if isinstance(layer, pytmx.TiledTileLayer):
                for x, y, gid in layer:
                    tile = ti(gid)
                    if tile:
                        rect = tile.get_rect()
                        tile = pygame.transform.scale(tile, (rect.width *
                                                             self.game.scale,
                                                             rect.height *
                                                             self.game.scale))
                        surface.blit(tile, (x * self.tmxdata.tilewidth *
                                            self.game.scale,
                                            y * self.tmxdata.tileheight *
                                            self.game.scale))

    def make_map(self):
        ''' Return a surface with all tiles rendered. '''
        temp_surface = pygame.Surface((self.width, self.height))
        self.render(temp_surface)
        return temp_surface


class Camera:
    ''' Screen camera controller. '''
    def __init__(self, width, height):
        ''' Init camera. '''
        self.camera = pygame.Rect(0, 0, width, height)
        self.width = width
        self.height = height

    def apply_entity(self, entity):
        ''' Move entity to the camera position. '''
        return entity.rect.move(self.camera.topleft)

    def apply_rect(self, rect):
        ''' Move rect to the camera position. '''
        return rect.move(self.camera.topleft)

    def update(self, game, pos):
        ''' Update the camera position. '''
        if game.player.rect.centerx < game.game_map.width and \
                game.player.rect.centerx > 0 and \
                game.player.rect.centery < game.game_map.height and \
                game.player.rect.centery > 0:
            row = 0
            col = 0
            x = pos[0]
            y = pos[1]
            size_x = game.game_map.width / game.game_map.levels_col
            size_y = game.game_map.height / game.game_map.levels_row
            while x > size_x:
                x -= size_x
                col += 1
            while y > size_y:
                y -= size_y
                row += 1
            if game.current_row != row or game.current_col != col:
                game.current_row = row
                game.current_col = col
            self.camera = pygame.Rect(-game.game_map.levels[row][col].x,
                                      -game.game_map.levels[row][col].y,
                                      self.width, self.height)
