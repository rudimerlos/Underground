# ---------------------------------------------------------------------------
#   Copyright (C) 2021  Rudi Merlos Carrión
#
#   This file is part of Underground.
#
#   Underground is free software: you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   Underground is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with Underground. If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------------

# ===========================================================================
#   GAME CLASS FOR UNDERGROUND GAME
# ===========================================================================

import pygame
import sys
from settings import *
from sprites import *
from tilemap import *
from os import path
from configparser import SafeConfigParser


class Game:
    ''' Game class. '''

    def __init__(self):
        ''' Init pygame and game variables. '''
        pygame.mixer.pre_init(44100, -16, 2, 512)
        pygame.display.init()
        pygame.mixer.init()
        pygame.font.init()
        pygame.joystick.init()
        self.conf_path = f'{path.dirname(path.realpath(__file__))}/config.cfg'
        self.read_config()
        self.width = WIDTH * self.scale
        self.height = HEIGHT * self.scale
        self.menu_height = MENU_BAR * self.scale
        self.total_height = self.height + self.menu_height
        self.screen = pygame.display.set_mode((self.width, self.total_height))
        self.status_bar = pygame.Surface((self.width, self.menu_height))
        self.status_bar_rect = self.status_bar.get_rect()
        pygame.display.set_caption(TITLE)
        pygame.mouse.set_visible(False)
        self.clock = pygame.time.Clock()
        self.running = True
        self.load_data()
        self.player = None
        self.playing = False
        self.paused = False

    def read_config(self):
        ''' Read config file. '''
        if path.exists(self.conf_path):
            config = SafeConfigParser()
            config.read(self.conf_path)
            self.scale = config.getint('GENERAL', 'size')
            self.fx_volume = config.getfloat('GENERAL', 'fx_volume') / 10
            self.music_volume = config.getfloat('GENERAL', 'music_volume') / 10
            self.keyboard = config.getboolean('GENERAL', 'keyboard')
            if not self.keyboard:
                joystick = pygame.joystick.get_count()
                if joystick <= 0:
                    self.keyboard = True
                else:
                    self.joystick = pygame.joystick.Joystick(0)
                    self.joystick.init()
            self.key_map = {}
            self.key_map['left'] = config.getint('KEYS', 'left')
            self.key_map['right'] = config.getint('KEYS', 'right')
            self.key_map['jump_long'] = config.getint('KEYS', 'jump_long')
            self.key_map['jump_short'] = config.getint('KEYS', 'jump_short')
            self.key_map['pause'] = config.getint('KEYS', 'pause')
            self.key_map['exit'] = config.getint('KEYS', 'exit')
        else:
            self.scale = 1
            self.fx_volume = .5
            self.music_volume = .8
            self.keyboard = True
            self.key_map = {
                'left': pygame.K_LEFT,
                'right': pygame.K_RIGHT,
                'jump_long': pygame.K_UP,
                'jump_short': pygame.K_DOWN,
                'pause': pygame.K_p,
                'exit': pygame.K_ESCAPE
            }
        self.new_scale = self.scale

    def write_config(self):
        ''' Write config file. '''
        config = SafeConfigParser()
        config['GENERAL'] = {
            'size': str(self.new_scale),
            'fx_volume': str(int(self.fx_volume * 10)),
            'music_volume': str(int(self.music_volume * 10)),
            'keyboard': str(self.keyboard)
        }
        config['KEYS'] = {
            'left': str(self.key_map['left']),
            'right': str(self.key_map['right']),
            'jump_long': str(self.key_map['jump_long']),
            'jump_short': str(self.key_map['jump_short']),
            'pause': str(self.key_map['pause']),
            'exit': str(self.key_map['exit'])
        }
        with open(self.conf_path, 'w') as f:
            config.write(f)

    def load_data(self):
        ''' Load game resources. '''
        root_dir = path.dirname(__file__)
        self.map_folder = path.join(root_dir, MAP_DIR)
        img_folder = path.join(root_dir, IMG_DIR)
        self.snd_folder = path.join(root_dir, SND_DIR)
        font_folder = path.join(root_dir, FONT_DIR)
        # LOAD IMAGES
        # Load spritesheet
        self.spritesheet = Spritesheet(self, path.join(img_folder,
                                                       SPRITESHEET))
        # MENU BAR
        # Load title
        self.title = pygame.image.load(path.join(img_folder, 'Underground.png')
                                       ).convert_alpha()
        self.title_rect = self.title.get_rect()
        self.title = pygame.transform.scale(self.title,
                                            (self.title_rect.width *
                                             self.scale,
                                             self.title_rect.height *
                                             self.scale))
        self.title_rect = self.title.get_rect()
        # Load lives
        self.live_img = self.spritesheet.get_image(PLAYER_IMG[1])
        self.live_rect = self.live_img.get_rect()
        self.live_img = pygame.transform.scale(self.live_img,
                                               (self.live_rect.width // 2,
                                                self.live_rect.height // 2))
        self.live_img.set_colorkey(BLACK)
        self.live_rect = self.live_img.get_rect()
        # Load keys
        self.keys_img = []
        keys = KEY_IMG.values()
        for key in keys:
            self.keys_img.append(self.spritesheet.get_image(key[0]))
        self.keys_rect = self.keys_img[2].get_rect()
        for key in self.keys_img:
            key.set_colorkey(BLACK)
        # MENU SCREEN
        self.menu_screen = pygame.image.load(path.join(img_folder,
                                                       'Main_menu.png')
                                             ).convert()
        self.ms_rect = self.menu_screen.get_rect()
        self.menu_screen = pygame.transform.scale(self.menu_screen,
                                                  (self.ms_rect.width *
                                                   self.scale,
                                                   self.ms_rect.height *
                                                   self.scale))
        self.ms_rect = self.menu_screen.get_rect()
        # LOAD SOUNDS
        self.init_snd = pygame.mixer.Sound(path.join(self.snd_folder, FX_INIT))
        self.init_snd.set_volume(self.fx_volume)
        self.walk_snd = []
        for snd in FX_WALK:
            self.walk_snd.append(pygame.mixer.Sound(path.join(self.snd_folder,
                                                              snd)))
        for snd in self.walk_snd:
            snd.set_volume(self.fx_volume)
        self.jump_short_snd = pygame.mixer.Sound(path.join(self.snd_folder,
                                                           FX_JUMP_SHORT))
        self.jump_short_snd.set_volume(self.fx_volume)
        self.jump_long_snd = pygame.mixer.Sound(path.join(self.snd_folder,
                                                          FX_JUMP_LONG))
        self.jump_long_snd.set_volume(self.fx_volume)
        self.hit_snd = pygame.mixer.Sound(path.join(self.snd_folder, FX_HIT))
        self.hit_snd.set_volume(self.fx_volume)
        self.key_snd = pygame.mixer.Sound(path.join(self.snd_folder, FX_KEY))
        self.key_snd.set_volume(self.fx_volume)
        self.door_snd = pygame.mixer.Sound(path.join(self.snd_folder, FX_DOOR))
        self.door_snd.set_volume(self.fx_volume)
        self.pause_snd = pygame.mixer.Sound(path.join(self.snd_folder,
                                                      FX_PAUSE))
        self.pause_snd.set_volume(self.fx_volume)
        self.item_live_snd = pygame.mixer.Sound(path.join(self.snd_folder,
                                                          FX_ITEM_LIVE))
        self.item_live_snd.set_volume(self.fx_volume)
        # LOAD FONTS
        self.font_normal = path.join(font_folder, FONT_NORMAL)
        self.font_bold = path.join(font_folder, FONT_BOLD)

    def load_tilemap(self, filename):
        ''' Load a level from tilemap. '''
        self.game_map = TileMap(self, path.join(self.map_folder, filename))
        self.map_img = self.game_map.make_map()
        self.map_rect = self.map_img.get_rect()
        for tile_object in self.game_map.tmxdata.objects:
            if tile_object.name == 'player':
                # If there is not player, he creates it
                if self.player is None:
                    self.player = Player(self, tile_object.x,
                                         tile_object.y + 16)
            elif tile_object.name == 'wall':
                Obstacle(self, tile_object.x, tile_object.y, tile_object.width,
                         tile_object.height)
            elif tile_object.name == 'checkpoint':
                Checkpoint(self, tile_object.x, tile_object.y,
                           tile_object.width, tile_object.height,
                           tile_object.type)
            elif tile_object.name == 'player_live':
                PlayerLive(self, tile_object.x, tile_object.y,
                           tile_object.type)
            elif tile_object.name == 'wall_thunder':
                BallThunder(self, tile_object.x, tile_object.y,
                            tile_object.width, tile_object.height)
            elif tile_object.name == 'ball':
                Ball(self, tile_object.color, tile_object.x, tile_object.y,
                     tile_object.range, tile_object.type)
            elif tile_object.name == 'death':
                Death(self, tile_object.x, tile_object.y, tile_object.width,
                      tile_object.height)
            elif tile_object.name == 'flybug':
                FlyBug(self, tile_object.color, tile_object.x, tile_object.y,
                       tile_object.range, tile_object.type)
            elif tile_object.name == 'skull':
                Skull(self, tile_object.color, tile_object.x, tile_object.y)
            elif tile_object.name == 'smog':
                Smog(self, tile_object.color, tile_object.x, tile_object.y,
                     tile_object.range, tile_object.type)
            elif tile_object.name == 'spider':
                Spider(self, tile_object.color, tile_object.x, tile_object.y,
                       tile_object.range, tile_object.type)
            elif tile_object.name == 'thunder':
                Thunder(self, tile_object.color, tile_object.x, tile_object.y)
            elif tile_object.name == 'water':
                Water(self, tile_object.color, tile_object.x, tile_object.y)
            elif tile_object.name == 'worm':
                Worm(self, tile_object.color, tile_object.x, tile_object.y,
                     tile_object.range, tile_object.type)
            elif tile_object.name == 'zombie':
                Zombie(self, tile_object.color, tile_object.x, tile_object.y,
                       tile_object.range, tile_object.type)
            elif tile_object.name == 'key':
                Key(self, tile_object.x, tile_object.y, tile_object.number)
            elif tile_object.name == 'door':
                Door(self, tile_object.x, tile_object.y, tile_object.number)

    def new(self):
        ''' Reset variables and start a new game. '''
        self.all_sprites = pygame.sprite.LayeredUpdates()
        self.walls = pygame.sprite.Group()
        self.checkpoints = pygame.sprite.Group()
        self.items = pygame.sprite.Group()
        self.mobs = pygame.sprite.Group()
        self.ball_thunders = pygame.sprite.Group()
        self.thunders = pygame.sprite.Group()
        self.deaths = pygame.sprite.Group()
        self.keys = pygame.sprite.Group()
        self.doors = pygame.sprite.Group()
        self.load_tilemap('map.tmx')
        self.camera = Camera(self.game_map.width, self.game_map.height)
        self.player_x = self.player.rect.centerx
        self.player_y = self.player.rect.bottom
        self.current_row = 0
        self.current_col = 0
        self.init_snd.play()
        self.run()

    def run(self):
        ''' Run a game. '''
        self.playing = True
        while self.playing:
            self.clock.tick(FPS)
            self.events()
            if self.player.win:
                self.end_game()
            if not self.paused:
                self.update()
            self.draw()
            if self.player.lives == 0:
                self.player = None

    def update(self):
        ''' Update game. '''
        self.all_sprites.update()
        # Update camera position
        self.camera.update(self, self.player.rect.center)
        # Hit checkpoint
        checkpoint_hit = pygame.sprite.spritecollide(self.player,
                                                     self.checkpoints, False)
        if checkpoint_hit:
            self.player_x = checkpoint_hit[0].rect.centerx
            self.player_y = checkpoint_hit[0].rect.bottom
            self.player_front = checkpoint_hit[0].front
        # Hit items
        live_hit = pygame.sprite.spritecollide(self.player, self.items, True,
                                               pygame.sprite.collide_mask)
        if live_hit:
            self.item_live_snd.play()
            self.player.lives += 1
        # Hit enemies
        enemy_hit = pygame.sprite.spritecollide(self.player, self.mobs, False,
                                                pygame.sprite.collide_mask)
        if enemy_hit:
            pygame.mixer.stop()
            self.hit_snd.play()
            self.player.reset_death()
        # Hit thunderbolt
        thunder_hit = pygame.sprite.spritecollide(self.player, self.thunders,
                                                  False,
                                                  pygame.sprite.collide_mask)
        if thunder_hit:
            pygame.mixer.stop()
            self.hit_snd.play()
            self.player.reset_death()
        # Hit death
        death_hit = pygame.sprite.spritecollide(self.player, self.deaths,
                                                False)
        if death_hit:
            pygame.mixer.stop()
            self.hit_snd.play()
            self.player.reset_death()
        # Hit keys
        key_hit = pygame.sprite.spritecollide(self.player, self.keys, True)
        if key_hit:
            pygame.mixer.stop()
            self.key_snd.play()
            self.player.keys.append(key_hit[0].number)
        # Hit door
        door_hit = pygame.sprite.spritecollide(self.player, self.doors, False)
        if door_hit:
            if door_hit[0].number in self.player.keys:
                pygame.mixer.stop()
                self.door_snd.play()
                self.player.keys.remove(door_hit[0].number)
                door_hit[0].kill()
            else:
                if self.player.vel.x > 0:
                    self.player.pos.x = door_hit[0].rect.left - \
                        self.player.rect.width / 2
                elif self.player.vel.x < 0:
                    self.player.pos.x = door_hit[0].rect.right + \
                        self.player.rect.width / 2
        # Player is death
        if self.player.lives == 0:
            self.playing = False
            self.player.kill()
        # Player is winner
        if self.player.rect.centerx > self.game_map.width or \
                self.player.rect.centerx < 0 or \
                self.player.rect.centery > self.game_map.height or \
                self.player.rect.centery < 0:
            self.player.win = True

    def events(self):
        ''' Event handler for game. '''
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                if self.playing:
                    self.playing = False
                self.running = False
                self.write_config()
            if event.type == pygame.KEYDOWN:
                # Game exit
                if event.key == self.key_map['exit']:
                    if self.paused:
                        self.pause_snd.play()
                        self.paused = not self.paused
                    else:
                        self.playing = False
                        self.player.lives = 0
                        self.player.kill()
                # Game pause
                elif event.key == self.key_map['pause']:
                    pygame.mixer.stop()
                    self.pause_snd.play()
                    self.paused = not self.paused
            if self.keyboard:
                if event.type == pygame.KEYDOWN:
                    # Player jump
                    if event.key == self.key_map['jump_long']:
                        if not self.player.jumping:
                            self.jump_long_snd.play()
                        self.player.jump(self.player.vel.x)
                    elif event.key == self.key_map['jump_short']:
                        if not self.player.jumping:
                            self.jump_short_snd.play()
                        self.player.jump(self.player.vel.x, True)
            else:
                if event.type == pygame.JOYBUTTONDOWN:
                    if self.joystick.get_button(0):
                        if not self.player.jumping:
                            self.jump_long_snd.play()
                        self.player.jump(self.player.vel.x)
                    elif self.joystick.get_button(1):
                        if not self.player.jumping:
                            self.jump_short_snd.play()
                        self.player.jump(self.player.vel.x, True)

    def draw_status_bar(self):
        ''' Draw status bar in the bottom of the screen. '''
        self.status_bar.fill(BLACK)
        for i in range(self.player.lives - 1):
            x = i * (self.live_rect.width / self.scale + 4) + 4
            y = 2
            if i > 6:
                x = (i - 7) * (self.live_rect.width / self.scale + 4) + 4
                y = (self.live_rect.height / self.scale + 2) + 1
            self.status_bar.blit(self.live_img, (x * self.scale,
                                                 y * self.scale))
        self.status_bar.blit(self.title, (self.status_bar_rect.centerx -
                                          self.title_rect.width / 2, 0))
        key = KEY_IMG.keys()
        pos = 10
        for k in key:
            if int(k) in self.player.keys:
                self.status_bar.blit(self.keys_img[int(k) - 1],
                                     (self.status_bar_rect.right -
                                      (pos * self.scale), 4 * self.scale))
                pos += 10
        self.screen.blit(self.status_bar,
                         (0, self.total_height - self.menu_height))

    def draw(self):
        ''' Draw sprites. '''
        # Draw screen
        self.screen.fill(BLACK)
        self.screen.blit(self.map_img,
                         self.camera.apply_rect(self.map_rect))
        for sprite in self.all_sprites:
            self.screen.blit(sprite.image, self.camera.apply_entity(sprite))
        self.draw_status_bar()
        # Draw pause screen
        if self.paused:
            self.pause_screen()
        pygame.display.flip()

    def menu(self):
        ''' Create and draw main menu. '''
        # Music menu
        pygame.mixer.music.load(path.join(self.snd_folder, MENU_MUSIC))
        pygame.mixer.music.play(loops=-1)
        pygame.mixer.music.set_volume(self.music_volume)
        pos_keyboard = ((140 * self.scale, 132 * self.scale),
                        (198 * self.scale, 132 * self.scale))
        pos_joystick = ((140 * self.scale, 147 * self.scale),
                        (206 * self.scale, 147 * self.scale))
        # Draw and events menu
        while not self.playing:
            self.clock.tick(FPS)
            self.screen.fill(BLACK)
            self.screen.blit(self.menu_screen, (0, 0))
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.write_config()
                    self.quit()
                if e.type == pygame.KEYUP:
                    if e.key == pygame.K_1:
                        self.playing = True
                    elif e.key == pygame.K_2:
                        self.keyboard = True
                    elif e.key == pygame.K_3:
                        joystick = pygame.joystick.get_count()
                        if joystick > 0:
                            self.joystick = pygame.joystick.Joystick(0)
                            self.joystick.init()
                            self.keyboard = False
                    elif e.key == pygame.K_4:
                        self.options_menu()
                    elif e.key == pygame.K_5:
                        self.write_config()
                        self.quit()
            if self.keyboard:
                pygame.draw.line(self.screen, YELLOW, pos_keyboard[0],
                                 pos_keyboard[1], 2 * self.scale)
            else:
                pygame.draw.line(self.screen, YELLOW, pos_joystick[0],
                                 pos_joystick[1], 2 * self.scale)
            pygame.display.flip()
        pygame.mixer.music.fadeout(500)

    def pause_screen(self):
        ''' Create and draw the pause screen. '''
        self.screen.fill(BLACK)
        self.draw_text(self.font_bold, 'PAUSE', 30, WHITE, 130 * self.scale,
                       self.height / 2)

    def options_menu(self):
        ''' Create and draw submenú for game options. '''
        exit = False
        while not exit:
            self.clock.tick(FPS)
            self.screen.fill(BLACK)
            self.draw_text(self.font_bold, '1 - REDEFINE TECLAS', 20, WHITE,
                           80 * self.scale, 60 * self.scale)
            self.draw_text(self.font_bold, '2 - VOLUMEN EFECTOS', 20, WHITE,
                           80 * self.scale, 80 * self.scale)
            self.draw_text(self.font_bold, '3 - VOLUMEN MÚSICA', 20, WHITE,
                           80 * self.scale, 100 * self.scale)
            self.draw_text(self.font_bold, '4 - TAMAÑO DE PANTALLA', 20, WHITE,
                           80 * self.scale, 120 * self.scale)
            self.draw_text(self.font_bold, '5 - MENÚ PRINCIPAL', 20, WHITE,
                           80 * self.scale, 140 * self.scale)
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.write_config()
                    self.quit()
                if e.type == pygame.KEYUP:
                    if e.key == pygame.K_1:
                        self.redefine_keys_menu()
                    elif e.key == pygame.K_2:
                        self.fx_volume_menu()
                    elif e.key == pygame.K_3:
                        self.music_volume_menu()
                    elif e.key == pygame.K_4:
                        self.screen_size()
                    elif e.key == pygame.K_5:
                        exit = True
            pygame.display.flip()

    def redefine_keys_menu(self):
        ''' Create and draw the redefine keys menu. '''
        exit = False
        # Key name for show text
        keyname = ''
        # List of keys needed
        keys = ['IZQUIERDA', 'DERECHA', 'SALTO LARGO', 'SALTO CORTO', 'PAUSA',
                'SALIR']
        key_map = ['left', 'right', 'jump_long', 'jump_short', 'pause', 'exit']
        # List where save key codes
        list_keys = []
        i = 0
        while not exit:
            self.screen.fill(BLACK)
            self.draw_text(self.font_bold, keys[i], 20, WHITE,
                           self.width / 2, 70 * self.scale, True)
            pygame.display.flip()
            event = pygame.event.wait()
            if event.type == pygame.QUIT:
                exit = True
            if event.type == pygame.KEYDOWN:
                # Key code
                key = event.key
                keyname = pygame.key.name(key)
                if keyname != '':
                    self.draw_text(self.font_bold, keyname.upper(), 20, WHITE,
                                   self.width / 2, 90 * self.scale, True)
                    list_keys.append(key)
                    pygame.display.flip()
                    i += 1
                    pygame.time.delay(500)
                    if i >= len(keys):
                        exit = True
        # Save in self.key_map the new key codes
        # i = 0
        for i in range(len(key_map)):
            self.key_map[key_map[i]] = list_keys[i]
        # for e in self.key_map.keys():
            # self.key_map[e] = list_keys[i]
            # i += 1

    def fx_volume_menu(self):
        ''' Create and draw the fx volume menu adjust. '''
        exit = False
        while not exit:
            self.clock.tick(FPS)
            self.screen.fill(BLACK)
            self.draw_text(self.font_bold, 'VOLUMEN DE EFECTOS', 20, WHITE,
                           90 * self.scale, 70 * self.scale)
            self.draw_text(self.font_bold, str(int(self.fx_volume * 10)), 20,
                           WHITE, self.width / 2, 110 * self.scale)
            self.draw_text(self.font_normal, 'ENTER PARA VOLVER', 20, WHITE,
                           90 * self.scale, 150 * self.scale)
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.quit()
                if e.type == pygame.KEYUP:
                    if e.key == pygame.K_UP:
                        self.fx_volume += .1
                        if self.fx_volume > 1:
                            self.fx_volume = 1
                        self.fx_volume_adjust()
                        self.hit_snd.play()
                    elif e.key == pygame.K_DOWN:
                        self.fx_volume -= .1
                        if self.fx_volume < 0:
                            self.fx_volume = 0
                        self.fx_volume_adjust()
                        self.hit_snd.play()
                    elif e.key == pygame.K_RETURN:
                        exit = True
            pygame.display.flip()

    def music_volume_menu(self):
        ''' Create and draw the music volumen menu adjust. '''
        exit = False
        while not exit:
            self.clock.tick(FPS)
            self.screen.fill(BLACK)
            self.draw_text(self.font_bold, 'VOLUMEN DE MÚSICA', 20, WHITE,
                           90 * self.scale, 70 * self.scale)
            self.draw_text(self.font_bold, str(int(self.music_volume * 10)),
                           20, WHITE, self.width / 2, 110 * self.scale)
            self.draw_text(self.font_normal, 'ENTER PARA VOLVER', 20, WHITE,
                           90 * self.scale, 150 * self.scale)
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.quit()
                if e.type == pygame.KEYUP:
                    if e.key == pygame.K_UP:
                        self.music_volume += .1
                        if self.music_volume > 1:
                            self.music_volume = 1
                        pygame.mixer.music.set_volume(self.music_volume)
                    elif e.key == pygame.K_DOWN:
                        self.music_volume -= .1
                        if self.music_volume < 0:
                            self.music_volume = 0
                        pygame.mixer.music.set_volume(self.music_volume)
                    elif e.key == pygame.K_RETURN:
                        exit = True
            pygame.display.flip()

    def screen_size(self):
        ''' Create and draw the screen size menu adjust. '''
        exit = False
        msg = ['PEQUEÑO', 'MEDIANO', 'GRANDE', 'MUY GRANDE']
        while not exit:
            self.clock.tick(FPS)
            self.screen.fill(BLACK)
            self.draw_text(self.font_bold, 'TAMAÑO DE PANTALLA', 20, WHITE,
                           self.width / 2, 70 * self.scale, True)
            self.draw_text(self.font_normal, 'ES NECESARIO REINICIAR EL JUEGO',
                           14, WHITE, self.width / 2, 85 * self.scale, True)
            self.draw_text(self.font_bold, msg[self.new_scale - 1], 20, WHITE,
                           self.width / 2, 110 * self.scale, True)
            self.draw_text(self.font_normal, 'ENTER PARA VOLVER', 20, WHITE,
                           self.width / 2, 150 * self.scale, True)
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.quit()
                if e.type == pygame.KEYUP:
                    if e.key == pygame.K_UP:
                        self.new_scale += 1
                        if self.new_scale > 4:
                            self.new_scale = 4
                    elif e.key == pygame.K_DOWN:
                        self.new_scale -= 1
                        if self.new_scale < 1:
                            self.new_scale = 1
                    elif e.key == pygame.K_RETURN:
                        exit = True
            pygame.display.flip()

    def fx_volume_adjust(self):
        ''' Adjust all fx volumes. '''
        self.init_snd.set_volume(self.fx_volume)
        for snd in self.walk_snd:
            snd.set_volume(self.fx_volume)
        self.jump_short_snd.set_volume(self.fx_volume)
        self.jump_long_snd.set_volume(self.fx_volume)
        self.hit_snd.set_volume(self.fx_volume)
        self.key_snd.set_volume(self.fx_volume)
        self.door_snd.set_volume(self.fx_volume)
        self.pause_snd.set_volume(self.fx_volume)

    def draw_text(self, font, text, size, color, x, y, center=False):
        ''' Blit a text on the screen with font, size, color and position
        args. '''
        font = pygame.font.Font(font, size * self.scale)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        if center:
            text_rect.midtop = (x, y)
        else:
            text_rect.topleft = (x, y)
        self.screen.blit(text_surface, text_rect)

    def end_game(self):
        ''' End game screen when the player is winner. '''
        pygame.mixer.music.load(path.join(self.snd_folder, FINAL_MUSIC))
        pygame.mixer.music.play(loops=-1)
        pygame.mixer.music.set_volume(self.music_volume)
        exit = False
        crozer_img = self.spritesheet.get_image(CROZER_IMG)
        crozer_rect = crozer_img.get_rect()
        colors = [BLUE, BLUE_B, RED, RED_B, MAGENTA, MAGENTA_B, GREEN, GREEN_B,
                  CYAN, CYAN_B, YELLOW, YELLOW_B, WHITE, WHITE_B]
        i = 0
        while not exit:
            self.clock.tick(FPS)
            self.screen.fill(BLACK)
            self.draw_text(self.font_bold, '¡¡¡ENHORABUENA!!!', 40, colors[i],
                           self.width / 2, 70 * self.scale, True)
            self.draw_text(self.font_normal, 'PULSA ENTER PARA SALIR', 15,
                           WHITE, self.width / 2, self.height - 10 *
                           self.scale, True)
            self.player.end_update()
            self.screen.blit(crozer_img,
                             (self.player.rect.right + 20 * self.scale,
                              self.player.rect.bottom + crozer_rect.height))
            pygame.display.flip()
            i += 1
            if i >= len(colors):
                i = 0
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.write_config()
                    self.quit()
                elif e.type == pygame.KEYUP:
                    if e.key == pygame.K_RETURN:
                        exit = True
                        self.player.lives = 0

    def quit(self):
        ''' Quit pygame and exit. '''
        pygame.quit()
        sys.exit()
