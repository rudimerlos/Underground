# UNDERGROUND

Underground es un juego de plataformas al más puro estilo Spectrum ZX programado en python con la biblioteca pygame.


### HISTORIA

Estamos en el año 2187 y el mundo es muy diferente al que conocemos. La contaminación ha llegado a límites de riesgo de muerte para toda la población. Un joven aventurero llamado Mike ha descubierto una posible solución. El crozer, un mineral que nace en las profundidades de la tierra y con el que pueden fabricar una cúpula para poder sobrevivir, pero no va a ser fácil. Para ello tendrá que introducirse en una peligrosa gruta que lo conducirá a un mundo hasta ahora desconocido por nosotros, lleno de terribles monstruos y seres extraños para los que Mike no es bienvenido. Mike tendrá que encontrar este extraño y escaso mineral abriéndose camino entre esta jungla subterránea y encontrando llaves para poder abrir los misteriosos pasadizos hasta llegar al preciado crozer. ¿Crees que podrás ayudarle?


### INSTALACIÓN

Underground puede ser ejecutado con el intérprete de python o directamente desde un ejecutable ya compilado. Hay que tener en cuenta que los binarios compilados tienen un mayor peso ya que integran todas las librerías necesarias para la correcta ejecución sin necesidad de tener instalado Python en el sistema.


#### EJECUCIÓN DESDE PYTHON

Para correr Underground es necesario tener instalado Python3 además de los módulos pygame y pytmx, los cuales se pueden instalar mediante pip.
Para ejecutarlo basta con, estando en el directorio Underground, escribir en la terminal:

    $ python3 main.py

Para instalaciones en GNU Linux o sistemas UNIX, también es posible ejecutar el script install.sh ubicado en el directorio principal. Este script comprueba todas las dependencias y, si todo es correcto, procede a la instalación.

Para desinstalar Underground basta con eliminar los siguientes archivos y directorios:

    $HOME/Underground
    $HOME/.local/bin/underground
    $HOME/.local/share/applications/underground.desktop


#### DESCARGA DE BINARIOS

Descarga el binario compilado [aquí](https://drive.google.com/drive/folders/1rEXF9TON0qZ9jSsK297TuiEjeoW5P45W?usp=sharing).


### AUTOR

Diseño y programación por Rudi Merlos Carrión.


### LICENCIA

Copyright (C) 2021  Rudi Merlos Carrión

This file is part of Underground.

Underground is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Underground is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Underground. If not, see <https://www.gnu.org/licenses/>.