#!/bin/bash

# print a color message
colormsg() {
    tput setaf $1;
    echo -e $2;
    tput sgr0;
}

# Vars
CURRENT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
HOME_DIR=$(cd $HOME && pwd)
VERSION="1.0.3"

# Check python3, pip and dependencies are installed
is_ok=1
if command -v python3 &>/dev/null; then
    colormsg 3 "$(python3 -V) \c"
    colormsg 2 "is installed"
    if command -v pip3 &>/dev/null; then
        colormsg 3 "pip $(pip3 -V | cut -d ' ' -f 2) \c"
        colormsg 2 "is installed"
        if pip3 show pygame &>/dev/null; then
            colormsg 3 "pygame $(pip3 show pygame | grep Version | cut -d ' ' -f 2) \c"
            colormsg 2 "is installed"
        else
            colormsg 3 "pygame \c"
            colormsg 1 "is not installed"
            is_ok=0
        fi
        if pip3 show pytmx &>/dev/null; then
            colormsg 3 "pytmx $(pip3 show pytmx | grep Version | cut -d ' ' -f 2) \c"
            colormsg 2 "is installed"
        else
            colormsg 3 "pytmx \c"
            colormsg 1 "is not installed"
            is_ok=0
        fi
    else
        colormsg 3 "pip3 \c"
        colormsg 1 "is not installed"
        is_ok=0
    fi
else
    colormsg 3 "Python3 \c"
    colormsg 1 "is not installed"
    is_ok=0
fi

if [ $is_ok -eq 1 ]; then
    colormsg 5 "Installing..."
    # Copy files
    if [ "$CURRENT_DIR" != "$HOME_DIR/Underground" ]; then
        cp -r $CURRENT_DIR $HOME_DIR/Underground
    fi

    # Create launcher
    if [ ! -d $HOME_DIR/.local/bin ]; then
        mkdir -p $HOME_DIR/.local/bin
    fi
    LAUNCHER_FILE=$HOME_DIR/.local/bin/underground
    echo "#!/bin/bash" >> $LAUNCHER_FILE
    echo "python3 $HOME/Underground/main.py" >> $LAUNCHER_FILE
    chmod +x $LAUNCHER_FILE

    colormsg 5 "Creating a desktop launcher..."
    # Create desktop launcher
    if [ ! -d $HOME_DIR/.local/share/applications ]; then
        mkdir -p $HOME_DIR/.local/share/applications
    fi
    DESKTOP_LAUNCHER_FILE=$HOME_DIR/.local/share/applications/underground.desktop
    echo "[Desktop Entry]" >> $DESKTOP_LAUNCHER_FILE
    echo "Version=$VERSION" >> $DESKTOP_LAUNCHER_FILE
    echo "Name=Underground" >> $DESKTOP_LAUNCHER_FILE
    echo "Exec=$HOME_DIR/.local/bin/underground" >> $DESKTOP_LAUNCHER_FILE
    echo "Icon=$HOME_DIR/Underground/icon.png" >> $DESKTOP_LAUNCHER_FILE
    echo "Terminal=false" >> $DESKTOP_LAUNCHER_FILE
    echo "Type=Application" >> $DESKTOP_LAUNCHER_FILE
    echo "Categories=Game" >> $DESKTOP_LAUNCHER_FILE

    colormsg 2 "Installation is succesfuly!!!"
fi
