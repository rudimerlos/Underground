# ---------------------------------------------------------------------------
#   Copyright (C) 2021  Rudi Merlos Carrión
#
#   This file is part of Underground.
#
#   Underground is free software: you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   Underground is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with Underground. If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------------

# ===========================================================================
#   SPRITES CLASSES FOR UNDERGROUND GAME
# ===========================================================================

import pygame
from settings import *
from game import *


class Spritesheet:
    ''' Utility class for loading and parsing spritesheets. '''
    def __init__(self, game, filename):
        self.spritesheet = pygame.image.load(filename).convert()
        self.game = game

    def get_image(self, coords):
        ''' Grab an image out of a larger spritesheets. '''
        # coords[0] = x  coords[1] = y  coords[2] = w  coords[3] = h
        image = pygame.Surface((coords[2], coords[3]))
        image.blit(self.spritesheet, (0, 0), (coords[0], coords[1],
                                              coords[2], coords[3]))
        image = pygame.transform.scale(image, (coords[2] * self.game.scale,
                                               coords[3] * self.game.scale))
        return image


class Player(pygame.sprite.Sprite):
    ''' Class Player inherit pygame.sprite.Sprite '''
    def __init__(self, game, x, y):
        ''' Init sprite class. '''
        self._layer = PLAYER_LAYER
        self.groups = game.all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.next_frame = -1
        self.current_frame = 1
        self.last_update = 0
        self.load_images()
        self.image = self.walk_frames_r[self.current_frame]
        self.rect = self.image.get_rect()
        # Sprite position
        self.pos = vec(x * self.game.scale + self.rect.width / 2, y *
                       self.game.scale)
        self.rect.centerx = self.pos.x
        self.rect.bottom = self.pos.y
        # Sprite speed
        self.vel = vec(0, 0)
        # Jump speed X and Y
        self.yj = 0
        self.xj = 0
        # Is enable while we are walking
        self.walking = False
        # Is enable while we are jumping
        self.jumping = False
        # Is enable while we are falling without having jumped
        self.falling = True
        # Stops speed in Y when jump hits a platform below
        self.stop_vy = False
        # Is enable when we are looks forward
        self.front = True
        # Lives
        self.lives = PLAYER_LIVES
        # Keys for doors
        self.keys = []
        # Player win
        self.win = False
        self.die = False

    def load_images(self):
        ''' Load images from spritesheet. '''
        self.walk_frames_r = []
        for e in PLAYER_IMG:
            self.walk_frames_r.append(self.game.spritesheet.get_image(e))
        for frame in self.walk_frames_r:
            frame.set_colorkey(BLACK)
        self.jump_frame_r = self.game.spritesheet.get_image(PLAYER_IMG[3])
        self.jump_frame_r.set_colorkey(BLACK)
        # Flip duplicate images
        self.walk_frames_l = []
        for frame in self.walk_frames_r:
            frame.set_colorkey(BLACK)
            self.walk_frames_l.append(
                pygame.transform.flip(frame, True, False))
        self.jump_frame_l = pygame.transform.flip(
            self.jump_frame_r, True, False)
        self.jump_frame_l.set_colorkey(BLACK)

    def get_keys(self):
        ''' Control keys for moving sprite. '''
        if self.game.keyboard:
            keys = pygame.key.get_pressed()
            # If not jumping and not falling we can to move horizontally
            if not self.jumping and not self.falling:
                if keys[self.game.key_map['left']]:
                    self.vel.x = -PLAYER_VEL * self.game.scale
                elif keys[self.game.key_map['right']]:
                    self.vel.x = PLAYER_VEL * self.game.scale
        else:
            # If not jumping and not falling we can to move horizontally
            if not self.jumping and not self.falling:
                if self.game.joystick.get_axis(0) < -0.5:
                    self.vel.x = -PLAYER_VEL * self.game.scale
                elif self.game.joystick.get_axis(0) > 0.5:
                    self.vel.x = PLAYER_VEL * self.game.scale

    def jump(self, vel, short=False):
        ''' Jump control. '''
        # If not jumping and not falling we can jump
        if not self.jumping and not self.falling:
            # If short == True short jump, if short == False long jump
            self.yj = (PLAYER_JUMP if not short else PLAYER_JUMP / 2) * \
                self.game.scale
            self.jumping = True
            # If it is moving horizontally the jump is lateral
            if vel > 0:
                self.xj = PLAYER_VEL * self.game.scale
            elif vel < 0:
                self.xj = -PLAYER_VEL * self.game.scale

    def collide_walls(self, d):
        ''' Collision with walls. '''
        hits = pygame.sprite.spritecollide(self, self.game.walls, False)
        if hits:
            # Lateral collisions with walls or platforms
            if d == 'x':
                if self.vel.x > 0:
                    self.pos.x = hits[0].rect.left - self.rect.width / 2
                elif self.vel.x < 0:
                    self.pos.x = hits[0].rect.right + self.rect.width / 2
                self.vel.x = 0
                self.rect.centerx = self.pos.x
            # Vertical collisions with walls or platforms
            if d == 'y':
                # If the collision is below it's because we are jumping and if
                # the collision is above it's because we have touched ground.
                # In both cases we are no longer falling
                self.falling = False
                if self.vel.y > 0:
                    self.jumping = False
                    self.pos.y = hits[0].rect.top
                    self.xj = 0
                    self.stop_vy = False
                elif self.vel.y < 0:
                    self.pos.y = hits[0].rect.bottom + self.rect.height
                    # If the collision is below we no longer increase the jump
                    # speed
                    self.stop_vy = True
                self.vel.y = 0
                self.rect.bottom = self.pos.y
            return True
        return False

    def animate(self):
        ''' Animate sprites. '''
        now = pygame.time.get_ticks()
        # We control if we are walking
        if self.vel.x == 0 or self.jumping or self.falling:
            self.walking = False
            if self.front:
                self.image = self.walk_frames_r[1]
            else:
                self.image = self.walk_frames_l[1]
        else:
            self.walking = True
        # Walking animation
        if self.walking:
            if now - self.last_update > PLAYER_ANIM:
                self.last_update = now
                if self.current_frame == 0:
                    self.game.walk_snd[0].play()
                    self.next_frame = 1
                elif self.current_frame == 2:
                    self.game.walk_snd[1].play()
                    self.next_frame = -1
                self.current_frame += self.next_frame
                if self.die:
                    self.die = False
                else:
                    if self.vel.x > 0:
                        self.image = self.walk_frames_r[self.current_frame]
                        self.front = True
                    else:
                        self.image = self.walk_frames_l[self.current_frame]
                        self.front = False
        elif self.jumping:
            if self.front:
                self.image = self.jump_frame_r
            else:
                self.image = self.jump_frame_l
        self.mask = pygame.mask.from_surface(self.image)

    def reset_death(self):
        ''' Reset some variables when lost a live. '''
        pygame.time.delay(500)
        self.vel = vec(0, 0)
        self.xj = 0
        self.yj = 0
        self.jumping = False
        self.pos.x = self.game.player_x
        self.pos.y = self.game.player_y
        self.rect.centerx = self.pos.x
        self.rect.bottom = self.pos.y
        self.lives -= 1
        self.front = self.game.player_front
        self.die = True

    def update(self):
        ''' Update sprite. '''
        self.animate()
        self.vel = vec(0, 0)
        # While yj is greater than 0 and does not hit with a higher platform
        # the jump speed increase
        if self.yj > 0:
            if not self.stop_vy:
                self.vel.y -= PLAYER_GRAV * self.game.scale
            self.yj -= 1 * self.game.scale
            self.vel.x = self.xj
        # But, the gravity acts
        else:
            self.vel.y = PLAYER_GRAV * self.game.scale
            self.vel.x = self.xj
        self.get_keys()
        # Update the position and check collisions with the platforms
        self.pos += self.vel
        self.rect.centerx = self.pos.x
        self.collide_walls('x')
        self.rect.bottom = self.pos.y
        # If we are not crashing in Y and we are not jumping, then we are
        # falling
        if not self.collide_walls('y') and not self.jumping:
            self.falling = True

    def end_animation(self):
        ''' Animation sprite when the player is winner. '''
        self.walking = True
        self.front = True
        now = pygame.time.get_ticks()
        if now - self.last_update > PLAYER_ANIM_END:
            self.last_update = now
            if self.current_frame == 0:
                self.next_frame = 1
            elif self.current_frame == 2:
                self.next_frame = -1
            self.current_frame += self.next_frame
            self.image = self.walk_frames_r[self.current_frame]

    def end_update(self):
        ''' Update sprite when the player is winner. '''
        self.end_animation()
        self.rect.centerx = self.game.width / 2 - self.rect.width
        self.rect.bottom = self.game.height * 3 / 4
        self.game.screen.blit(self.image, (self.rect.centerx,
                                           self.rect.bottom))


class Checkpoint(pygame.sprite.Sprite):
    ''' Class Checkpoint inherit pygame.sprite.Sprite. '''
    def __init__(self, game, x, y, w, h, front=False):
        self.groups = game.checkpoints
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.x = x * self.game.scale
        self.y = y * self.game.scale
        self.rect = pygame.Rect(self.x, self.y, w * self.game.scale, h *
                                self.game.scale)
        self.rect.x = self.x
        self.rect.y = self.y
        self.front = front


class PlayerLive(pygame.sprite.Sprite):
    ''' Class PlayerLive inherit pygame.sprite.Sprite. '''
    def __init__(self, game, x, y, front=False):
        self._layer = ITEM_LAYER
        self.groups = game.all_sprites, game.items
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.image = self.game.spritesheet.get_image(PLAYER_IMG[1])
        self.rect = self.image.get_rect()
        self.image = pygame.transform.scale(self.image,
                                            (self.rect.width // 2,
                                             self.rect.height // 2))
        if not front:
            self.image = pygame.transform.flip(self.image, True, False)
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.pos = vec(x * self.game.scale, y * self.game.scale)
        self.mask = pygame.mask.from_surface(self.image)

    def update(self):
        ''' Update sprite position. '''
        self.rect.topleft = self.pos


class Obstacle(pygame.sprite.Sprite):
    ''' Rectangle of tilemap obstacle. '''
    def __init__(self, game, x, y, w, h):
        self._layer = WALL_LAYER
        self.groups = game.walls
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.x = x * self.game.scale
        self.y = y * self.game.scale
        self.rect = pygame.Rect(self.x, self.y, w * self.game.scale, h *
                                self.game.scale)
        self.rect.x = self.x
        self.rect.y = self.y


class BallThunder(Obstacle):
    ''' Class BallThunder inherit Obstacle. '''
    def __init__(self, game, x, y, w, h):
        groups = game.ball_thunders
        pygame.sprite.Sprite.__init__(self, groups)
        Obstacle.__init__(self, game, x, y, w, h)


class Death(pygame.sprite.Sprite):
    ''' Class Death inherit pygame.sprite.Sprite. '''
    def __init__(self, game, x, y, w, h):
        self._layer = WALL_LAYER
        self.groups = game.deaths
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.x = x * self.game.scale
        self.y = y * self.game.scale
        self.rect = pygame.Rect(self.x, self.y, w * self.game.scale, h *
                                self.game.scale)
        self.rect.x = self.x
        self.rect.y = self.y


class Thunder(pygame.sprite.Sprite):
    ''' Class Thunder inherit pygame.sprite.Sprite. '''
    def __init__(self, game, coords, x, y):
        self._layer = MOB_LAYER
        self.groups = game.all_sprites, game.thunders
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.current_frame = -1
        self.last_update = 0
        self.coords = THUNDER_IMG[coords]
        self.load_images()
        self.image = self.frames[self.current_frame]
        self.rect = self.image.get_rect()
        # Sprite position
        self.pos = vec(x * self.game.scale, y * self.game.scale)
        self.direction = 1

    def load_images(self):
        ''' Load images from spritesheet. '''
        self.frames = []
        for e in self.coords:
            self.frames.append(self.game.spritesheet.get_image(e))
        for frame in self.frames:
            frame.set_colorkey(BLACK)

    def animate(self):
        ''' Animate sprites. '''
        now = pygame.time.get_ticks()
        if now - self.last_update > THUNDER_ANIM:
            self.last_update = now
            self.current_frame += 1
            if self.current_frame == THUNDER_FRAMES:
                self.current_frame = 0
            self.image = self.frames[self.current_frame]
            self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)

    def update(self):
        ''' Update sprite. '''
        self.animate()
        self.rect.left = self.pos.x
        self.pos.y += THUNDER_VEL * self.game.scale * self.direction
        self.rect.top = self.pos.y
        hit = pygame.sprite.spritecollide(self, self.game.ball_thunders, False)
        if hit:
            self.direction *= -1


class SDMob(pygame.sprite.Sprite):
    ''' Generic class for simple direction mobs. '''
    def __init__(self, game, coords, x, y, rang):
        self._layer = MOB_LAYER
        self.groups = game.all_sprites, game.mobs
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.current_frame = -1
        self.next_frame = 1
        self.last_update = 0
        self.coords = coords
        self.load_images()
        self.image = self.frames[self.current_frame]
        self.rect = self.image.get_rect()
        # Sprite position
        self.pos = vec(x * self.game.scale, y * self.game.scale)
        # Sprite motion
        self.path = 0
        self.rang = rang * TILE

    def load_images(self):
        ''' Load images from spritesheet. '''
        self.frames = []
        for e in self.coords:
            self.frames.append(self.game.spritesheet.get_image(e))
        for frame in self.frames:
            frame.set_colorkey(BLACK)

    def animate(self, anim, frames, ddframes=False):
        ''' Animate sprites. ddframes arg set a double direction frames for
        animation. '''
        now = pygame.time.get_ticks()
        if now - self.last_update > anim:
            self.last_update = now
            self.current_frame += self.next_frame
            if ddframes:
                if self.current_frame == frames or self.current_frame == -1:
                    self.next_frame *= -1
                    self.current_frame += self.next_frame
                    if self.current_frame == 0:
                        self.current_frame += 1
                    else:
                        self.current_frame -= 1
            else:
                if self.current_frame == frames:
                    self.current_frame = 0
            self.image = self.frames[self.current_frame]
            self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)


class Skull(SDMob):
    ''' Class Skull inherit SDMob. '''
    def __init__(self, game, coords, x, y):
        SDMob.__init__(self, game, SKULL_IMG[coords], x, y, 0)

    def update(self):
        ''' Update sprites. '''
        self.animate(SKULL_ANIM, SKULL_FRAMES)
        self.rect.top = self.pos.y
        self.rect.left = self.pos.x


class Spider(SDMob):
    ''' Class Spider inherit SDMob. '''
    def __init__(self, game, coords, x, y, rang, direct):
        SDMob.__init__(self, game, SPIDER_IMG[coords], x, y, int(rang))
        self.direction = -1 if direct == 'invert' else 1

    def update(self):
        ''' Update sprites. '''
        self.animate(SPIDER_ANIM, SPIDER_FRAMES, True)
        self.rect.left = self.pos.x
        self.pos.y += SPIDER_VEL * self.game.scale * self.direction
        self.rect.top = self.pos.y
        self.path += SPIDER_VEL
        if self.path > self.rang:
            self.path = 0
            self.direction *= -1


class Ball(SDMob):
    ''' Class Ball inherit SDMob. '''
    def __init__(self, game, coords, x, y, rang, direct):
        SDMob.__init__(self, game, BALL_IMG[coords], x, y, int(rang))
        self.direction = 1 if direct == 'invert' else -1
        self.rang_v = 5
        self.path_v = 0
        self.direction_v = 1

    def update(self):
        ''' Update sprites. '''
        self.animate(BALL_ANIM, BALL_FRAMES, True)
        self.pos.x += BALL_VEL * self.game.scale * self.direction
        self.pos.y += BALL_VEL_V * self.game.scale * self.direction_v
        self.rect.center = self.pos
        self.path += BALL_VEL
        self.path_v += BALL_VEL_V
        if self.path > self.rang:
            self.path = 0
            self.direction *= -1
        if self.path_v > self.rang_v:
            self.path_v = 0
            self.direction_v *= -1


class Smog(SDMob):
    ''' Class Smog inherit SDMob. '''
    def __init__(self, game, coords, x, y, rang, type):
        SDMob.__init__(self, game, SMOG_IMG[coords], x, y, int(rang))
        # self.direction = -1
        self.direction = -1 if type == 'horizontal' else 1
        self.rang_v = 2
        self.path_v = 0
        self.direction_v = 1
        self.type = type

    def update(self):
        ''' Update sprites. '''
        self.animate(SMOG_ANIM, SMOG_FRAMES)
        if self.type == 'horizontal':
            self.pos.x += SMOG_VEL * self.game.scale * self.direction
            self.pos.y += SMOG_VEL_V * self.game.scale * self.direction_v
        elif self.type == 'vertical':
            self.pos.y += SMOG_VEL * self.game.scale * self.direction
            self.pos.x += SMOG_VEL_V * self.game.scale * self.direction_v
        self.rect.center = self.pos
        self.path += SMOG_VEL
        self.path_v += SMOG_VEL_V
        if self.path > self.rang:
            self.path = 0
            self.direction *= -1
        if self.path_v > self.rang_v:
            self.path_v = 0
            self.direction_v *= -1


class Water(SDMob):
    ''' Class Water inherit SDMob. '''
    def __init__(self, game, coords, x, y):
        SDMob.__init__(self, game, WATER_IMG[coords], x, y, 0)

    def update(self):
        ''' Update sprites. '''
        self.animate(WATER_ANIM, WATER_FRAMES)
        self.rect.top = self.pos.y
        self.rect.left = self.pos.x


class DDMob(pygame.sprite.Sprite):
    ''' Generic class for double direction mobs. '''
    def __init__(self, game, coords, x, y, rang, direct):
        self._layer = MOB_LAYER
        self.groups = game.all_sprites, game.mobs
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.current_frame = -1
        self.last_update = 0
        self.coords = coords
        self.load_images()
        self.image = self.walk_frames_r[self.current_frame]
        self.rect = self.image.get_rect()
        # Sprite position
        self.pos = vec(x * self.game.scale, y * self.game.scale)
        # Sprite motion
        self.path = 0
        # self.rang = MOB_H_PATH[rang]
        self.rang = rang * TILE
        self.direction = 1 if direct == 'invert' else -1

    def load_images(self):
        ''' Load images from spritesheet. '''
        self.walk_frames_r = []
        for e in self.coords:
            self.walk_frames_r.append(self.game.spritesheet.get_image(e))
        for frame in self.walk_frames_r:
            frame.set_colorkey(BLACK)
        # Flip duplicate images
        self.walk_frames_l = []
        for frame in self.walk_frames_r:
            frame.set_colorkey(BLACK)
            self.walk_frames_l.append(
                pygame.transform.flip(frame, True, False))

    def animate(self, anim, frames):
        ''' Animate sprites. '''
        now = pygame.time.get_ticks()
        if now - self.last_update > anim:
            self.last_update = now
            self.current_frame += 1
            if self.current_frame == frames:
                self.current_frame = 0
            if self.direction > 0:
                self.image = self.walk_frames_r[self.current_frame]
            else:
                self.image = self.walk_frames_l[self.current_frame]
            self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)


class Zombie(DDMob):
    ''' Class Zombie inherit DDMob. '''
    def __init__(self, game, coords, x, y, rang, direct):
        DDMob.__init__(self, game, ZOMBIE_IMG[coords], x, y, int(rang), direct)

    def update(self):
        ''' Update sprites. '''
        self.animate(ZOMBIE_ANIM, ZOMBIE_FRAMES)
        self.rect.top = self.pos.y
        self.pos.x += ZOMBIE_VEL * self.game.scale * self.direction
        self.rect.left = self.pos.x
        self.path += ZOMBIE_VEL
        if self.path > self.rang:
            self.path = 0
            self.direction *= -1


class FlyBug(DDMob):
    ''' FlyBug inherit DDMob. '''
    def __init__(self, game, coords, x, y, rang, direct):
        DDMob.__init__(self, game, FLYBUG_IMG[coords], x, y, int(rang), direct)

    def update(self):
        ''' Update sprites. '''
        self.animate(FLYBUG_ANIM, FLYBUG_FRAMES)
        self.rect.top = self.pos.y
        self.pos.x += FLYBUG_VEL * self.game.scale * self.direction
        self.rect.left = self.pos.x
        self.path += FLYBUG_VEL
        if self.path > self.rang:
            self.path = 0
            self.direction *= -1


class Worm(DDMob):
    ''' Worm inherit DDMob. '''
    def __init__(self, game, coords, x, y, rang, direct):
        DDMob.__init__(self, game, WORM_IMG[coords], x, y, int(rang), direct)

    def update(self):
        ''' Update sprites. '''
        self.animate(WORM_ANIM, WORM_FRAMES)
        self.rect.top = self.pos.y
        self.pos.x += WORM_VEL * self.game.scale * self.direction
        self.rect.left = self.pos.x
        self.path += WORM_VEL
        if self.path > self.rang:
            self.path = 0
            self.direction *= -1


class Key(pygame.sprite.Sprite):
    ''' Class Key inherit pygame.sprite.Sprite '''
    def __init__(self, game, x, y, number):
        self._layer = KEY_LAYER
        self.groups = game.all_sprites, game.keys
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.number = number
        self.current_frame = -1
        self.next_frame = 1
        self.last_update = 0
        self.load_images()
        self.image = self.frames[self.current_frame]
        self.rect = self.image.get_rect()
        # Sprite position
        self.pos = vec(x * self.game.scale, y * self.game.scale)

    def load_images(self):
        ''' Load frames from spritesheet. '''
        self.frames = []
        for e in KEY_IMG[str(self.number)]:
            self.frames.append(self.game.spritesheet.get_image(e))
        self.frames.append(pygame.transform.flip(self.frames[0], True, False))
        for frame in self.frames:
            frame.set_colorkey(BLACK)

    def animate(self):
        ''' Animate sprites. '''
        now = pygame.time.get_ticks()
        if now - self.last_update > KEY_ANIM:
            self.last_update = now
            self.current_frame += self.next_frame
            if self.current_frame == KEY_FRAMES or self.current_frame == -1:
                self.next_frame *= -1
                self.current_frame += self.next_frame
                if self.current_frame == 0:
                    self.current_frame += 1
                else:
                    self.current_frame -= 1
            self.image = self.frames[self.current_frame]
            self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)

    def update(self):
        ''' Update sprites. '''
        self.animate()
        self.rect.topleft = self.pos


class Door(pygame.sprite.Sprite):
    ''' Class Door inherit pygame.sprite.Sprite. '''
    def __init__(self, game, x, y, number):
        self._layer = DOOR_LAYER
        self.groups = game.all_sprites, game.doors
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        self.number = number
        self.current_frame = -1
        self.last_update = 0
        self.load_images()
        self.image = self.frames[self.current_frame]
        self.rect = self.image.get_rect()
        # Sprite position
        self.pos = vec(x * game.scale, y * game.scale)

    def load_images(self):
        ''' Load frames from spritesheet. '''
        self.frames = []
        for e in DOOR_IMG[str(self.number)]:
            self.frames.append(self.game.spritesheet.get_image(e))
        for frame in self.frames:
            frame.set_colorkey(BLACK)

    def animate(self):
        ''' Animate sprites. '''
        now = pygame.time.get_ticks()
        if now - self.last_update > DOOR_ANIM:
            self.last_update = now
            self.current_frame += 1
            if self.current_frame == DOOR_FRAMES:
                self.current_frame = 0
            self.image = self.frames[self.current_frame]
            self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)

    def update(self):
        ''' Update sprites. '''
        self.animate()
        self.rect.topleft = self.pos
