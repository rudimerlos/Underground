# ---------------------------------------------------------------------------
#   Copyright (C) 2021  Rudi Merlos Carrión
#
#   This file is part of Underground.
#
#   Underground is free software: you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the Free
#   Software Foundation, either version 3 of the License, or (at your option)
#   any later version.
#
#   Underground is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along
#   with Underground. If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------------

# ===========================================================================
#   SETTINGS FOR UNDERGROUND GAME
# ===========================================================================

import pygame

# Global
vec = pygame.math.Vector2

# General settings
TITLE = 'Underground'
WIDTH = 320
HEIGHT = 200
MENU_BAR = 20
FPS = 50
TILE = 8

# Path settings
IMG_DIR = 'assets/img'
FONT_DIR = 'assets/fonts'
MAP_DIR = 'assets/maps'
MUSIC_DIR = 'assets/music'
SND_DIR = 'assets/snd'
FONT_NORMAL = 'zx_spectrum-7.ttf'
FONT_BOLD = 'zx_spectrum-7_bold.ttf'
SPRITESHEET = 'Spritesheet.png'

# Define colors
BLACK = (0, 0, 0)
BLUE = (0, 0, 215)
BLUE_B = (0, 0, 255)
RED = (215, 0, 0)
RED_B = (255, 0, 0)
MAGENTA = (215, 0, 215)
MAGENTA_B = (255, 0, 255)
GREEN = (0, 215, 0)
GREEN_B = (0, 255, 0)
CYAN = (0, 215, 215)
CYAN_B = (0, 255, 255)
YELLOW = (215, 215, 0)
YELLOW_B = (255, 255, 0)
WHITE = (215, 215, 215)
WHITE_B = (255, 255, 255)

# Layers
WALL_LAYER = 1
KEY_LAYER = 1
MOB_LAYER = 2
ITEM_LAYER = 2
PLAYER_LAYER = 3
DOOR_LAYER = 4

# Player settings
PLAYER_IMG = [[384, 85, 14, 16], [399, 85, 14, 16], [384, 102, 14, 16],
              [399, 102, 14, 16]]
PLAYER_VEL = 1
PLAYER_GRAV = 1
PLAYER_JUMP = 40
PLAYER_ANIM = 60
PLAYER_ANIM_END = 140
PLAYER_LIVES = 5

# MOB SETTINGS
# Ball
BALL_IMG = {'blue': [[1, 6, 4, 4], [7, 5, 6, 6], [15, 4, 8, 8],
                     [25, 2, 12, 12], [39, 0, 16, 16]],
            'red': [[1, 23, 4, 4], [7, 22, 6, 6], [15, 21, 8, 8],
                    [25, 19, 12, 12], [39, 17, 16, 16]],
            'magenta': [[1, 40, 4, 4], [7, 39, 6, 6], [15, 38, 8, 8],
                        [25, 36, 12, 12], [39, 34, 16, 16]],
            'green': [[1, 57, 4, 4], [7, 56, 6, 6], [15, 55, 8, 8],
                      [25, 53, 12, 12], [39, 51, 16, 16]],
            'cyan': [[1, 74, 4, 4], [7, 73, 6, 6], [15, 72, 8, 8],
                     [25, 70, 12, 12], [39, 68, 16, 16]],
            'yellow': [[1, 91, 4, 4], [7, 90, 6, 6], [15, 89, 8, 8],
                       [25, 87, 12, 12], [39, 85, 16, 16]],
            'white': [[1, 108, 4, 4], [7, 107, 6, 6], [15, 106, 8, 8],
                      [25, 104, 12, 12], [39, 102, 16, 16]]}
BALL_VEL = .2
BALL_VEL_V = .3
BALL_ANIM = 120
BALL_FRAMES = 5
# FlyBug
FLYBUG_IMG = {'blue': [[210, 28, 16, 14], [227, 28, 16, 14],
                       [244, 28, 16, 14]],
              'red': [[210, 43, 16, 14], [227, 43, 16, 14],
                      [244, 43, 16, 14]],
              'magenta': [[210, 58, 16, 14], [227, 58, 16, 14],
                          [244, 58, 16, 14]],
              'green': [[210, 73, 16, 14], [227, 73, 16, 14],
                        [244, 73, 16, 14]],
              'cyan': [[210, 88, 16, 14], [227, 88, 16, 14],
                       [244, 88, 16, 14]],
              'yellow': [[210, 103, 16, 14], [227, 103, 16, 14],
                         [244, 103, 16, 14]],
              'white': [[210, 118, 16, 14], [227, 118, 16, 14],
                        [244, 118, 16, 14]]}
FLYBUG_VEL = .5
FLYBUG_ANIM = 100
FLYBUG_FRAMES = 3
# Skull
SKULL_IMG = {'blue': [[56, 0, 13, 17], [70, 0, 13, 17]],
             'red': [[56, 17, 13, 17], [70, 17, 13, 17]],
             'magenta': [[56, 36, 13, 17], [70, 36, 13, 17]],
             'green': [[56, 54, 13, 17], [70, 54, 13, 17]],
             'cyan': [[56, 72, 13, 17], [70, 72, 13, 17]],
             'yellow': [[56, 90, 13, 17], [70, 90, 13, 17]],
             'white': [[56, 108, 13, 17], [70, 108, 13, 17]]}
SKULL_ANIM = 200
SKULL_FRAMES = 2
# Smog
SMOG_IMG = {'blue': [[261, 28, 12, 12], [274, 28, 12, 12],
                     [287, 28, 12, 12]],
            'red': [[261, 41, 12, 12], [274, 41, 12, 12],
                    [287, 41, 12, 12]],
            'magenta': [[261, 54, 12, 12], [274, 54, 12, 12],
                        [287, 54, 12, 12]],
            'green': [[261, 67, 12, 12], [274, 67, 12, 12],
                      [287, 67, 12, 12]],
            'cyan': [[261, 80, 12, 12], [274, 80, 12, 12],
                     [287, 80, 12, 12]],
            'yellow': [[261, 93, 12, 12], [274, 93, 12, 12],
                       [287, 93, 12, 12]],
            'white': [[261, 106, 12, 12], [274, 106, 12, 12],
                      [287, 106, 12, 12]]}
SMOG_VEL = .3
SMOG_VEL_V = .1
SMOG_ANIM = 120
SMOG_FRAMES = 3
# Spider
SPIDER_IMG = {'blue': [[299, 28, 16, 12], [316, 28, 16, 12],
                       [333, 28, 16, 12]],
              'red': [[299, 41, 16, 12], [316, 41, 16, 12],
                      [333, 41, 16, 12]],
              'magenta': [[299, 54, 16, 12], [316, 54, 16, 12],
                          [333, 54, 16, 12]],
              'green': [[299, 67, 16, 12], [316, 67, 16, 12],
                        [333, 67, 16, 12]],
              'cyan': [[299, 80, 16, 12], [316, 80, 16, 12],
                       [333, 80, 16, 12]],
              'yellow': [[299, 93, 16, 12], [316, 93, 16, 12],
                         [333, 93, 16, 12]],
              'white': [[299, 106, 16, 12], [316, 106, 16, 12],
                        [333, 106, 16, 12]]}
SPIDER_VEL = .4
SPIDER_ANIM = 180
SPIDER_FRAMES = 3
# Worm
WORM_IMG = {'blue': [[382, 21, 16, 6], [382, 28, 16, 6], [382, 35, 16, 6]],
            'red': [[399, 21, 16, 6], [399, 28, 16, 6], [399, 36, 16, 6]],
            'magenta': [[382, 42, 16, 6], [382, 49, 16, 6], [382, 56, 16, 6]],
            'green': [[399, 42, 16, 6], [399, 49, 16, 6], [399, 56, 16, 6]],
            'cyan': [[382, 63, 16, 6], [382, 70, 16, 6], [382, 77, 16, 6]],
            'yellow': [[399, 63, 16, 6], [399, 70, 16, 6], [399, 77, 16, 6]],
            'white': [[399, 0, 16, 6], [399, 7, 16, 6], [399, 14, 16, 6]]}
WORM_VEL = .2
WORM_ANIM = 150
WORM_FRAMES = 3
# Zombie
ZOMBIE_IMG = {'blue': [[420, 0, 12, 16], [433, 0, 12, 16],
                       [446, 0, 12, 16], [459, 0, 12, 16]],
              'red': [[420, 17, 12, 16], [433, 17, 12, 16],
                      [446, 17, 12, 16], [459, 17, 12, 16]],
              'magenta': [[420, 34, 12, 16], [433, 34, 12, 16],
                          [446, 34, 12, 16], [459, 34, 12, 16]],
              'green': [[420, 51, 12, 16], [433, 51, 12, 16],
                        [446, 51, 12, 16], [459, 51, 12, 16]],
              'cyan': [[420, 68, 12, 16], [433, 68, 12, 16],
                       [446, 68, 12, 16], [459, 68, 12, 16]],
              'yellow': [[420, 85, 12, 16], [433, 85, 12, 16],
                         [446, 85, 12, 16], [459, 85, 12, 16]],
              'white': [[420, 102, 12, 16], [433, 102, 12, 16],
                        [446, 102, 12, 16], [459, 102, 12, 16]]}
ZOMBIE_VEL = .5
ZOMBIE_ANIM = 60
ZOMBIE_FRAMES = 4

# OBSTACLES
# Water
WATER_IMG = {'blue': [[209, 0, 16, 6], [209, 7, 16, 6], [209, 14, 16, 6],
                      [209, 21, 16, 6]],
             'red': [[226, 0, 16, 6], [226, 7, 16, 6], [226, 14, 16, 6],
                     [226, 21, 16, 6]],
             'magenta': [[243, 0, 16, 6], [243, 7, 16, 6], [243, 14, 16, 6],
                         [243, 21, 16, 6]],
             'green': [[260, 0, 16, 6], [260, 7, 16, 6], [260, 14, 16, 6],
                       [260, 21, 16, 6]],
             'cyan': [[277, 0, 16, 6], [277, 7, 16, 6], [277, 14, 16, 6],
                      [277, 21, 16, 6]],
             'yellow': [[294, 0, 16, 6], [294, 7, 16, 6], [294, 14, 16, 6],
                        [294, 21, 16, 6]],
             'white': [[311, 0, 16, 6], [311, 7, 16, 6], [311, 14, 16, 6],
                       [311, 21, 16, 6]]}
WATER_ANIM = 200
WATER_FRAMES = 4
# Thunderbolt
THUNDER_IMG = {'blue': [[352, 26, 6, 12], [359, 26, 6, 12],
                        [366, 26, 6, 12], [373, 26, 6, 12]],
               'red': [[352, 39, 6, 12], [359, 39, 6, 12],
                       [366, 39, 6, 12], [373, 39, 6, 12]],
               'magenta': [[352, 52, 6, 12], [359, 52, 6, 12],
                           [366, 52, 6, 12], [373, 52, 6, 12]],
               'green': [[352, 65, 6, 12], [359, 65, 6, 12],
                         [366, 65, 6, 12], [373, 65, 6, 12]],
               'cyan': [[352, 78, 6, 12], [359, 78, 6, 12],
                        [366, 78, 6, 12], [373, 78, 6, 12]],
               'yellow': [[352, 91, 6, 12], [359, 91, 6, 12],
                          [366, 91, 6, 12], [373, 91, 6, 12]],
               'white': [[352, 104, 6, 12], [359, 104, 6, 12],
                         [366, 104, 6, 12], [373, 104, 6, 12]]}
THUNDER_VEL = 1.2
THUNDER_ANIM = 150
THUNDER_FRAMES = 4

# KEYS AND DOORS
# Keys
KEY_IMG = {'1': [[203, 13, 5, 12], [203, 0, 5, 12]],
           '2': [[203, 26, 5, 12], [203, 0, 5, 12]],
           '3': [[203, 52, 7, 12], [203, 39, 7, 12]],
           '4': [[203, 65, 7, 12], [203, 39, 7, 12]],
           '5': [[203, 78, 7, 12], [203, 39, 7, 12]],
           '6': [[203, 91, 7, 12], [203, 39, 7, 12]],
           '7': [[203, 104, 7, 12], [203, 39, 7, 12]]}
KEY_ANIM = 200
KEY_FRAMES = 3
# Doors
DOOR_IMG = {'1': [[84, 0, 16, 16], [84, 17, 16, 16], [84, 34, 16, 16],
                  [84, 51, 16, 16], [84, 68, 16, 16], [84, 85, 16, 16],
                  [84, 102, 16, 16]],
            '2': [[101, 0, 16, 16], [101, 17, 16, 16], [101, 34, 16, 16],
                  [101, 51, 16, 16], [101, 68, 16, 16], [101, 85, 16, 16],
                  [101, 102, 16, 16]],
            '3': [[118, 0, 16, 16], [118, 17, 16, 16], [118, 34, 16, 16],
                  [118, 51, 16, 16], [118, 68, 16, 16], [118, 85, 16, 16],
                  [118, 102, 16, 16]],
            '4': [[135, 0, 16, 16], [135, 17, 16, 16], [135, 34, 16, 16],
                  [135, 51, 16, 16], [135, 68, 16, 16], [135, 85, 16, 16],
                  [135, 102, 16, 16]],
            '5': [[152, 0, 16, 16], [152, 17, 16, 16], [152, 34, 16, 16],
                  [152, 51, 16, 16], [152, 68, 16, 16], [152, 85, 16, 16],
                  [152, 102, 16, 16]],
            '6': [[169, 0, 16, 16], [169, 17, 16, 16], [169, 34, 16, 16],
                  [169, 51, 16, 16], [169, 68, 16, 16], [169, 85, 16, 16],
                  [169, 102, 16, 16]],
            '7': [[186, 0, 16, 16], [186, 17, 16, 16], [186, 34, 16, 16],
                  [186, 51, 16, 16], [186, 68, 16, 16], [186, 85, 16, 16],
                  [186, 102, 16, 16]]}
DOOR_ANIM = 50
DOOR_FRAMES = 7
# Crozer
CROZER_IMG = [384, 12, 8, 8]

# Sounds
MENU_MUSIC = 'Underground.ogg'
FINAL_MUSIC = 'Final.ogg'
FX_PAUSE = 'Pause.wav'
FX_INIT = 'Init.wav'
FX_WALK = ['Walk1.wav', 'Walk2.wav']
FX_JUMP_LONG = 'Jump_long.wav'
FX_JUMP_SHORT = 'Jump_short.wav'
FX_HIT = 'Hit.wav'
FX_KEY = 'Key.wav'
FX_DOOR = 'Door.wav'
FX_ITEM_LIVE = 'Item_live.wav'
